<?php

/*
 Template Name: Landing Page
 */

 ?>
<?php get_header(); ?>

<script type="text/javascript">
function initMap() 
{

    var mapCanvas1 = document.getElementById('map1');
    var mapOptions1 = {
      center: new google.maps.LatLng(29.9961875,30.9120067),
      // center: new google.maps.LatLng(31.691361, 30.101870),
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map1 = new google.maps.Map(mapCanvas1, mapOptions1);
    var position_1 = {lat: 29.9961875, lng: 30.9120067};
    // var position_1 = {lat: 31.691361, lng: 30.101870};

    var marker_1 = new google.maps.Marker({
      position: position_1,
      map: map1,
      title: 'Garden Hills',
      icon: $('.marker_url').val()
    });

}
</script>

<?php

$new_url = do_shortcode('[urlparam param="source"/]');
if(empty($new_url))
$new_url = '';

// print_r($new_url);

$video = get_field('video', get_the_ID());

$c5_club_array = array();
$args = array( 'numberposts' => -1, 'post_type'=>'c5_club', 'suppress_filters' => 0);
$c5_club = get_posts( $args );
foreach ( $c5_club as $post ) :   setup_postdata( $post );

$title = $post -> post_title;

$banner = wp_get_attachment_url( get_post_thumbnail_id($post -> ID) );
$image = get_field('image', $post -> ID);
$text = get_field('text', $post -> ID);

$c5_club_array[] = array('banner' => $banner, 'image' => $image, 'title' => $title, 'text' => $text);

endforeach;
wp_reset_postdata();

// print_r($c5_club_array);


$perspectives_array = array();
$args = array( 'numberposts' => -1, 'post_type'=>'perspectives', 'suppress_filters' => 0,'orderby'=> 'date','order'=> 'ASC');
$perspectives = get_posts( $args );
foreach ( $perspectives as $post ) :   setup_postdata( $post );

$thumb = get_field('thumb', $post -> ID);
$image = get_field('image', $post -> ID);

$perspectives_array[] = array('thumb' => $thumb, 'image' => $image);

endforeach;
wp_reset_postdata();


$floor_array = array();
$args = array( 'numberposts' => -1, 'post_type'=>'floor_plans', 'suppress_filters' => 0,'orderby'=> 'date','order'=> 'ASC');
$floors = get_posts( $args );
foreach ( $floors as $post ) :   setup_postdata( $post );

$thumb = get_field('thumb', $post -> ID);
$image = get_field('image', $post -> ID);

$floor_array[] = array('thumb' => $thumb, 'image' => $image);

endforeach;
wp_reset_postdata();



$partners_array = array();
$args = array( 'numberposts' => -1, 'post_type'=>'partners', 'suppress_filters' => 0);
$partners = get_posts( $args );
foreach ( $partners as $post ) :   setup_postdata( $post );

$banner = wp_get_attachment_url( get_post_thumbnail_id($post -> ID) );

$partners_array[] = $banner;


endforeach;
wp_reset_postdata();

// print_r($partners_array);


$const_array = array();
$args = array( 'numberposts' => -1, 'post_type'=>'construction_updates', 'suppress_filters' => 0);
$const = get_posts( $args );
foreach ( $const as $post ) :   setup_postdata( $post );

$banner = wp_get_attachment_url( get_post_thumbnail_id($post -> ID) );
$image = get_field('image', $post -> ID);

$const_array[] = array('thumb' => $banner, 'image' => $image);


endforeach;
wp_reset_postdata();

// print_r($const_array);

$file_dl = get_field('file', $post -> ID);


?>
<link href="style_home_8.css?ver=1.4" rel="stylesheet" media="screen">


<title><?php echo $page_title; ?></title>
<body>

<!-- Google Code for SARAI Conversion Page -->
<script type="text/javascript">

</script>

<div class="overlay" id="loading">
  <div class="sk-circle">
  <div class="sk-circle1 sk-child"></div>
  <div class="sk-circle2 sk-child"></div>
  <div class="sk-circle3 sk-child"></div>
  <div class="sk-circle4 sk-child"></div>
  <div class="sk-circle5 sk-child"></div>
  <div class="sk-circle6 sk-child"></div>
  <div class="sk-circle7 sk-child"></div>
  <div class="sk-circle8 sk-child"></div>
  <div class="sk-circle9 sk-child"></div>
  <div class="sk-circle10 sk-child"></div>
  <div class="sk-circle11 sk-child"></div>
  <div class="sk-circle12 sk-child"></div>
</div>
</div>

<div class="outer_container container">
  <div class="top_div" id="home">
    <nav class="navbar navbar-default navbar-fixed-top mobile_nav">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand mobile_links" href="#home" >
              <!-- <img slideID="home" class="n_logo menu_spans" src="assets/img/c5_logo.jpg"/> -->
            </a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="#prop" slideID="prop" class="menu_spans">Properties</a></li>
              <li><a href="#c5_club" slideID="c5_club" class="menu_spans">C5 Club</a></li>
              <li><a href="#cons_updates" slideID="cons_updates" class="menu_spans">Construction Updates</a></li>
              <li><a href="#location" slideID="location" class="menu_spans">Location</a></li>
              <li><a href="#partners" slideID="partners" class="menu_spans">Partners</a></li>
              <li><a href="#contact_us" slideID="contact_us" class="menu_spans">Contact Us</a></li>
              <li><a href="#register" slideID="register" class="menu_spans">Register</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
    <div class="header_div container">
      <div class="col-sm-1">
      </div>
      <div class="col-sm-10 head_spans">
        <span class="menu_span" slideID="prop">Properties | </span>
        <span class="menu_span" slideID="c5_club">C5 Club | </span>
        <span class="menu_span" slideID="cons_updates">Construction Updates | </span>
        <span class="menu_span" slideID="location">Location | </span>
        <span class="menu_span" slideID="partners">Partners | </span>
        <span class="menu_span" slideID="contact_us">Contact Us | </span>
        <span class="menu_span" slideID="register">Register</span>
        <a href="tel:19513"><span class="menu_span" slideID="register"><img class="phone_head" src="assets/img/phone_head.png">19513</span></a>
      </div>
      <div class="col-sm-1">
      </div>
    </div>
    <div class="top_div_main">
      <!-- <img class="top_div_main_img_1" src="assets/img/banner.jpg"/> -->
      <img class="top_div_main_img_2" src="assets/img/c5_logo.jpg"/>
    </div>
    <div class="top_div_text">
      <span class="top_div_text_span_2">Garden Hills</span>
    </div>
    <div class="top_div_text_2" id="about_us">
      <p class="top_div_text_2_p">
        We believe that we have an obligation to contribute to our country’s welfare. Through the efforts of the company we strive to tangibly improve the lives and living conditions of our society. As active corporate citizens, we’ve learned that investing in communities -- with the goal of making a real difference -- is not easy. There are many good causes, many strong organizations and many people who could benefit from additional resources. Choosing where to invest can be difficult. Through our many years of experience, however, we’ve found that one characteristic can determine the success; this is why
      </p>
      <p class="top_div_text_2_p top_div_text_2_p_2">
        we introduce to you our latest premium project C5 in the heart of Garden Hills project, 6th of October…
      </p>
    </div>
  </div>
  <div class="slider_div">
    <?php
    echo do_shortcode('[rev_slider alias="top_slider"]');
    ?>
  </div>
  <div class="prop_div module" id="prop">
    <div class="prop_div_span_1_div">
      <span class="prop_div_span_1">Properties</span>
    </div>
    <div class="prop_div_span_2_div">
      <span class="prop_div_span_2 prop_div_span_2_1">Perspectives</span>
      <span class="prop_div_span_2"> |</span>
      <span class="prop_div_span_2 prop_div_span_2_2">Floor Plans</span>
    </div>
    <div class="prop_slider container">
      <div id="owl-demo" class="owl-carousel owl-theme owl-demo pres_owl">
        <?php
          foreach ($perspectives_array as $key => $value) {
            
            ?>
            <div class="item row_1_item">
              <img class="floor_img" src="<?php echo $value['thumb']; ?>"/>
              <div class="hidden_image_div">
                <img class="const_img" src="<?php echo $value['image']; ?>"/>
              </div>
            </div>
            <?php
          }
          ?>
      </div>
      <div id="owl-demo" class="owl-carousel owl-theme owl-demo floor_owl hidden_slider">
        <?php
          foreach ($floor_array as $key => $value) {
            
            ?>
            <div class="item row_1_item">
              <img class="floor_img" src="<?php echo $value['thumb']; ?>"/>
              <div class="hidden_image_div">
                <img class="const_img" src="<?php echo $value['image']; ?>"/>
              </div>
            </div>
            <?php
          }
          ?>
      </div>
    </div>
    <div class="prop_bot_div">
      <span class="prop_bot_div_span">The heart of 6th of October</span>
      <p class="prop_bot_div_p prop_bot_div_p_1">
        Perfectly located in the heart of 6th of October, C5 is where all the hype is happening! 
      </p>
      <p class="prop_bot_div_p prop_bot_div_p_2">
Overlooking breathtaking greenery spread on over 150 feddans, every member of the family will find tranquility and serenity of a true home atmosphere…
      </p>
      <div class="download_bro_div">
        <a href="<?php echo $file_dl; ?>" download>
          <button type="button" class="btn btn-default">Download Brochure</button>      
        </a>
      </div>
    </div>
  </div>
  <div class="club_main_bg">
    <img src="assets/img/club_bg.jpg"/>
  </div>
  <div class="amen_div container module" id="c5_club">
    <div class="amen_div_span_1_div">
      <span class="amen_div_span_1">Facilities & Services</span>
    </div>
    <div class="prop_div_span_2_div">
      <span class="prop_div_span_2 prop_div_span_2_1">SOMETHING FOR EVERYONE</span>
    </div>
    <div id="owl-demo" class="owl-carousel owl-theme owl-demo_2">
        <?php
        foreach ($c5_club_array as $key => $value) {
          
          ?>
          <div class="item row_1_item serv_item">
            <span class="ser_spans"><?php echo $value['title']; ?></span>
            <img src="<?php echo $value['banner']; ?>"/>
            <p><?php echo $value['text']; ?></p>
            <!-- <div class="hidden_image_div">
              <?php if(!empty($value['text']))
              {
                ?>
                <span><?php echo $value['title']; ?></span>
                <?php
              }
              ?>
              <img class="const_img" src="<?php echo $value['image']; ?>"/>
              <?php if(!empty($value['text']))
              {
                ?>
                <p><?php echo $value['text']; ?></p>
                <?php
              }
              ?>
            </div> -->
          </div>
          <?php
        }
        ?>
    </div>
    <p class="amen_p">
        With wide open spaces, sports courts, and numerous options of exercising, every family member will find a convenient, healthy treat for his health, body and mind. Getting involved with your little ones in this fun, developmentally appropriate concept of C5 will ensure a positive and active lifestyle. C5 will make you live healthy, have fun and above all connect together as a family, along with your neighbors and C5 residents.
      </p>
  </div>
  <div class="const_div module" id="cons_updates">
    <div class="const_div_span_1_div">
      <span class="const_div_span_1">Construction Updates</span>
    </div>
    <div class="const_div_span_2_div">
      <span class="const_div_span_2">August 2016</span>
    </div>
    <div class="container">
      <div id="owl-demo" class="owl-carousel owl-theme owl-demo const_owl">
        <?php
          foreach ($const_array as $key => $value) {
            
            ?>
            <div class="item row_1_item">
              <img class="const_img" src="<?php echo $value['thumb']; ?>"/>
              <div class="hidden_image_div">
                <img class="const_img" src="<?php echo $value['image']; ?>"/>
              </div>
            </div>
            <?php
          }
          ?>
      </div>
    </div>
  </div>
  <div class="location_div module" id="location">
    <div class="location_div_span_1_div">
      <span class="location_div_span_1">Location</span>
    </div>
    <div class="location_div_span_2_div">
      <span class="location_div_span_2">Everything is Within Reach</span>
    </div>
    <div class="location_div_p_div">
      <p class="location_div_p location_div_p_1">Located 20 KM from Lebanon Square –Mohandeseen, and surrounded by Sheikh Zayed City and Cairo/Alex Road. C5 is the optimum spot in 6th of October. The whole project is located near touristic spots and service projects such as Carrefour, Hayper One Market, and other commercial malls, all within reach, along with many educational organizations (schools and universities) and necessary facilities.</p>
      <p class="location_div_p location_div_p_2">- 20 KM from Lebanon Square –Mohandeseen</p>
      <p class="location_div_p location_div_p_3">- Surrounded by both: Sheikh Zayed City and Cairo/Alex Road</p>
    </div>
    <div class="maps_div">
      <div id="map1" class="maps">
        <!-- <img src="assets/img/loc_bg.jpg"> -->
      </div>
    </div>
  </div>
  <div class="partners_div module" id="partners">
    <div class="partners_div_span_1_div">
      <span class="partners_div_span_1">Partners</span>
    </div>
    <div class="partners_div_images_div">
      <?php
      foreach ($partners_array as $key => $value) {
        
        ?>
        <img class="part_image" src="<?php echo $value; ?>">
        <?php
      }
      ?>
    </div>
    <div class="partners_div_images_div_2">
      <img src="assets/img/partners/owner.png">
    </div>
  </div>
  <div class="location_div module" id="contact_us">
    <div class="location_div_span_1_div">
      <span class="location_div_span_1">Contact Us</span>
    </div>
    <div class="location_div_span_1_div">
      <img class="phone_img" src="assets/img/phone.png"><a href="tel:19513"><span class="location_div_span_1 location_div_span_num">19513</span></a>
    </div>
    <div class="location_div_p_div">
      <p class="location_div_p location_div_p_1"><span class="cont_span">Head office</span>: 112 Mohy El Dien Abo El Ezz, Dokky, Giza.
<span class="cont_span_2">TEL:33365107- 33386805- 33386812, FAX: 33365107</span></p>
      <p class="location_div_p location_div_p_2"><span class="cont_span">Zamalek Branch</span>: 10 EL Kamel Mohamed St. Apartment NO.203, off 26th of July, Zamalek, Cairo.
<span class="cont_span_2">TEL:27372316/7/8/9</span></p>
    </div>
  </div>
  <div class="reg_div module" id="register">
    <div class="reg_frame">
      <span class="reg_div_span">Register Here</span>
      <form class="reg_form" formID="2">
      <div class="form-group">
        <input inputID="1" type="text" class="form-control form_inputs" placeholder="Name" required>
      </div>
      <div class="form-group">
        <input inputID="2" type="text" class="form-control form_inputs" placeholder="Mobile" required>
      </div>
      <div class="form-group">
        <input inputID="3" type="email" class="form-control form_inputs" placeholder="Email" required>
      </div>
      <div class="form-group">
        <input inputID="4" type="text" class="form-control form_inputs" placeholder="Country of Residence" required>
      </div>
      <div class="form-group">
        <select inputID="6" type="text" class="form-control form_inputs select_input" required>
          <option value="">Interested In Unit</option>
          <option value="120 m2">120 m2</option>
          <option value="130 m2">130 m2</option>
          <option value="140 m2">140 m2</option>
          <option value="185 m2">185 m2</option>
          <option value="200 m2">200 m2</option>
          <option value="240 m2">240 m2</option>
          <option value="260 m2">260 m2</option>
        </select>
      </div>

      <input inputID="5" type="hidden" value="<?php echo $new_url;?>" name="source" class="form_inputs">
      <button type="submit" class="btn btn-default submit_btn">Register</button>
    </form>
    </div>
  </div>
  <div class="footer_div footer container">
    <img class="menu_span top_arrow" slideID="home" src="assets/img/top_arrow.png"/>
    <div class="col-sm-3">
    </div>
    <div class="col-sm-6">
      <span class="footer_div_span">All rights reserved. EAC <?php echo date('Y')?></span>
      <div class="footer_icons">
        <a href="https://www.facebook.com/C5-Garden-Hills-1044298032349795/" target="_blank">
          <img src="assets/img/fb_icon.png"/>
        </a>
      </div>
    </div>
    <div class="col-sm-3 footer_last">
      
    </div>
  </div>
</div>


<button class="md-trigger md_trigger_sending" data-modal="modal-2"></button>

<button class="md-trigger md_trigger_image" data-modal="modal-4"></button>

<button class="md-trigger md_trigger_service" data-modal="modal-5"></button>

<div class="md-modal md-effect-2" id="modal-2">
  <div class="md-content">
    
  </div>
  <button class="md-close">X</button>
</div>


<div class="md-modal md-effect-22" id="modal-4">
  <div class="md-content">
    
  </div>
  <button class="md-close">X</button>
</div>

<div class="md-modal md-effect-22" id="modal-5">
  <div class="md-content">
    
  </div>
  <button class="md-close">X</button>
</div>

<div class="md-overlay"></div>
<link rel="stylesheet" type="text/css" href="assets/modal/component.css" />
<script async src="assets/modal/modernizr.custom.js"></script>
<script async src="assets/modal/classie.js"></script>
<script async src="assets/modal/modalEffects.js"></script>

<!-- <input type="hidden" class="thank_you_url" value="<?php echo get_permalink( 95 ); ?>"> -->
<input type="hidden" class="thank_you_url" value="<?php echo get_permalink( 75 ); ?>">

<?php get_footer(); ?>


</body>

<script  type="text/javascript" src="assets/owl_carousel_assets/owl.carousel.js"></script>
<script  type="text/javascript" src="assets/js/imagelightbox.js"></script>
<script async type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCe8ySOOlQeodi3WENjiXmopN24CvJEsto"></script>

<script>

var new_height = 0;

(function($) {

  $.fn.visible = function(partial) {
    
      var $t            = $(this),
          $w            = $(window),
          viewTop       = $w.scrollTop(),
          viewBottom    = viewTop + $w.height(),
          _top          = $t.offset().top,
          _bottom       = _top + $t.height(),
          compareTop    = partial === true ? _bottom : _top,
          compareBottom = partial === true ? _top : _bottom;
    
    return ((compareBottom <= viewBottom) && (compareTop >= viewTop));

  };
    
})(jQuery);

$( document ).ready(function() {
// $('.md_trigger_sending').trigger('click');
  var owl = $(".pres_owl");
  owl.owlCarousel({
     
     // items : 5,
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1300,3], //5 items between 1000px and 901px
      itemsDesktopSmall : [1130,3], // betweem 900px and 601px
      itemsTablet: [897,2], //2 items between 600 and 0
      itemsMobile : [608,1], // itemsMobile disabled - inherit from itemsTablet option
      navigation : true,
      autoPlay: 2000
      // scrollPerPage : true
  });

  var owl = $(".floor_owl");
  owl.owlCarousel({
     
     // items : 5,
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1300,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [1130,3], // betweem 900px and 601px
      itemsTablet: [897,2], //2 items between 600 and 0
      itemsMobile : [608,1], // itemsMobile disabled - inherit from itemsTablet option
      navigation : true,
      autoPlay: 2000
      // scrollPerPage : true
  });

  var owl = $(".const_owl");
  owl.owlCarousel({
     
     // items : 5,
      items : 3, //10 items above 1000px browser width
      itemsDesktop : [1300,3], //5 items between 1000px and 901px
      itemsDesktopSmall : [1130,3], // betweem 900px and 601px
      itemsTablet: [897,2], //2 items between 600 and 0
      itemsMobile : [608,1], // itemsMobile disabled - inherit from itemsTablet option
      navigation : true,
      autoPlay: 2000
      // scrollPerPage : true
  });


  var owl = $(".owl-demo_2");
  owl.owlCarousel({
     
     // items : 5,
      items : 3, //10 items above 1000px browser width
      itemsDesktop : [1400,3], //5 items between 1000px and 901px
      itemsDesktopSmall : [1130,2], // betweem 900px and 601px
      itemsTablet: [797,1], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
      navigation : true,
      autoPlay: 2000
      // autoPlay: 2000
      // scrollPerPage : true
  });




// $('.serv_item').on('click',function (e) {

//     e.preventDefault();
//     e.stopPropagation();

//     $('#modal-5 .md-content').html($(this).parent().find('.hidden_image_div').html());

//     $('.md-close').trigger('click');
//     $('.md_trigger_service').trigger('click');

//   });

$('.floor_img').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    $('#modal-4 .md-content').html($(this).parent().find('.hidden_image_div').html());

    $('.md-close').trigger('click');
    $('.md_trigger_image').trigger('click');

  });

$('.const_img').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    $('#modal-5 .md-content').html($(this).parent().find('.hidden_image_div').html());

    $('.md-close').trigger('click');
    $('.md_trigger_service').trigger('click');

  });

  $('.prop_div_span_2_1').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    $('.pres_owl').removeClass('hidden_slider');
    $('.floor_owl').addClass('hidden_slider');

  });

  $('.prop_div_span_2_2').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    $('.floor_owl').removeClass('hidden_slider');
    $('.pres_owl').addClass('hidden_slider');

  });

  $('.menu_span').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    var slideID = $(this).attr('slideID');
    var post = $('#'+slideID);

    var new_height_2 = parseInt($('.header_div').css('height'));

    var position = post.position().top - $(window).scrollTop();
    $('html, body').stop().animate({
        'scrollTop': post.offset().top - new_height_2
    }, 900, 'swing', function () {
        
    });

  });

  $('.menu_spans').on('click',function (e) {

    e.preventDefault();
    e.stopPropagation();

    $('.navbar-toggle').trigger('click');

    var slideID = $(this).attr('slideID');
    var post = $('#'+slideID);
  

    var position = post.position().top - $(window).scrollTop();
    $('html, body').stop().animate({
        'scrollTop': post.offset().top - new_height
    }, 900, 'swing', function () {
        
    });

  });

  $( ".reg_form" ).submit(function( event ) {
    
    event.preventDefault();

    return false;

  });

  var win = $(window);
var allMods = $(".module");

// Already visible modules
allMods.each(function(i, el) {
  var el = $(el);
  if (el.visible(true)) {
    el.addClass("already-visible"); 
  } 
});

win.scroll(function(event) {
  
  allMods.each(function(i, el) {
    var el = $(el);
    if (el.visible(true)) {
      el.addClass("come-in"); 
    } 
  });

  var scroll = $(window).scrollTop();
    if(scroll == 0)
    {
      $('.header_div').removeClass('top_head_class');
    }
    else
    {
      $('.header_div').addClass('top_head_class');
    }
  
});

});

window.onload = function() {


$('.overlay').fadeOut(500);
$('.outer_container').fadeIn(750);
$('.owl-demo_2 .item p').css('width', $('.owl-demo_2 .item img').css('width'));

initMap();

new_height = parseInt($('.navbar-header').css('height'));
console.log(new_height);

}

$(window).resize(function () {

    $('.owl-demo_2 .item p').css('width', $('.owl-demo_2 .item img').css('width'));
    new_height = parseInt($('.navbar-header').css('height'));
    // $('.owl-demo_2 .item p').css('width', $('.demo_img').css('width'));
});

$(window).scroll(function (event) {
    
});

</script>