<?php

/*
 Template Name: Thank You
 */

 ?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>

<!-- <script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script> -->
<script type="text/javascript" src="assets/js/jquery-2.1.3.min.js"></script>

<link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="assets/fancybox-videos_2/lib/jquery-1.10.1.min.js"></script> --> 

<link href="assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" media="screen">

<script type="text/javascript" src="assets/js/bootstrap-submenu.min.js"></script>
<script type="text/javascript" src="assets/js/docs.js"></script>

<script type="text/javascript" src="assets/js/visible.js"></script>

<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/owl_carousel_assets/owl.carousel.js"></script>

<!-- <link href="assets/css/bootstrap-submenu.min.css" rel="stylesheet" media="screen"> -->

<!-- Facebook Pixel Code -->

<!-- End Facebook Pixel Code -->

<?php wp_head() ?>
</head>


<link href="style_home_2.css" rel="stylesheet" media="screen">

<style type="text/css">

.outer_container
{
  display: block;
}

.top_div_main
{
  padding-top: 11%;
  padding-bottom: 30px;
}

.top_div_text_2_span_1 
{
  width: 80%;
  margin-left: auto;
  margin-right: auto;
  text-transform: inherit;
  color: #fff;
  font-family: 'Didot';
}



.top_div_text_2 
{
  padding-top: 5%;
  padding-bottom: 6%;
}

.footer_div_span
{
  font-family: 'Didot';
}

@media screen and (max-width: 500px) {

.top_div_text_2_span_1 
{
  font-size: 15px;
}

.top_div_text_2
{
  padding-top: 4%;
}

}

</style>
<title><?php echo $page_title; ?></title>
<body>

<!-- Google Code for Garden Hills Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 869374922;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "StxACOyUuWsQyrfGngM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/869374922/?label=StxACOyUuWsQyrfGngM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<div class="outer_container container">
  <div class="top_div" id="home">
    <div class="top_div_main">
      <img src="assets/img/c5_logo.jpg"/>
    </div>
    <div class="top_div_text_2 module" id="about_us">
      <span class="top_div_text_2_span_1">Thank you for your registeration. Our sales team will get in touch with you soon.</span>
    </div>
  </div>
  <div class="footer_div footer container">
    <div class="col-sm-3">
    </div>
    <div class="col-sm-6">
      <span class="footer_div_span">All rights reserved. EAC <?php echo date('Y')?></span>
      <div class="footer_icons">
        <a href="https://www.facebook.com/C5-Garden-Hills-1044298032349795/" target="_blank">
          <img src="assets/img/fb_icon.png"/>
        </a>
        <!-- <a href="https://www.instagram.com/sarai_newcairo/" target="_blank"> -->
          <img src="assets/img/insta_icon.png"/>
        <!-- </a> -->
      </div>
    </div>
    <div class="col-sm-3 footer_last">
    </div>
  </div>
</div>



<?php get_footer(); ?>


</body>
