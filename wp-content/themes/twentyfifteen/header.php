<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width,initial-scale=1.0">

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>

<!-- <script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script> -->
<script  type="text/javascript" src="assets/js/jquery-2.1.3.min.js"></script>

<link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
<script async type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="assets/fancybox-videos_2/lib/jquery-1.10.1.min.js"></script> -->	

<link href="assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" media="screen">

<!-- <script type="text/javascript" src="assets/js/bootstrap-submenu.min.js"></script>
<script type="text/javascript" src="assets/js/docs.js"></script> -->



<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/js/visible.js"></script>

<!-- <link href="assets/css/bootstrap-submenu.min.css" rel="stylesheet" media="screen"> -->


<?php wp_head() ?>
</head>

